﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    [Header("Setup")]
    public GameObject playerPrefab; //prefab for the player

    [Header("Values")]
    public float playerMoveSpeed; //player movement speed
    public float playerTurnSpeed; //player turn speed

    [Header("UI")]
    public Text currentScore; //text for score
    public Text highScore; //text for highscore

    private GameObject player; //player object
    private int score; //current score

    private void Start() //called in the beginning
    {
        SpawnPlayer();
    }

    /// <summary>
    /// called by player when found food
    /// </summary>
    public void FoundFood()
    {
        score++; //add score
        currentScore.text = score.ToString(); //update text
    }
    
    /// <summary>
    /// called when player dies
    /// </summary>
    public void PlayerDead()
    {
        SpawnPlayer(); //spawn new player
        if (score > int.Parse(highScore.text)) //if our new score is more than previous high score
            highScore.text = score.ToString(); //update text
        currentScore.text = "0"; //reset text
        score = 0;  //reset value
    }

    private void SpawnPlayer()
    {
        if (player != null) //if we have a player already
            Destroy(player); //destroy it
        player = Instantiate(playerPrefab, transform); //new player object
        player.GetComponent<PlayerSnake>().Init(this,playerMoveSpeed, playerTurnSpeed); //initialize it
        player.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360))); //set rotation to random
    }
}
