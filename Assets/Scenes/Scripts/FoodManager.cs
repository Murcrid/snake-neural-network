﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodManager : MonoBehaviour {

    [Header("Setup")]
    public GameObject foodPrefab; //the prefab for the foods
    [Header("Values")]
    public int foodPerSnake; //how many foods each individual snake sees

    [Header("Field Size")]
    public float mapSizeX = 10.0f; //map X size
    public float mapSizeY = 6.0f; //map Y size

    public float mapXOffset = 0.0f; //the offset for x value
    public float mapYOffset = -1.0f; //the offset for y value

    private List<GameObject> foodsNotInUse = new List<GameObject>();

    public void InitializeFoods(int populationCount)
    {
        for (int i = 0; i < populationCount * foodPerSnake; i++)
        {
            GameObject food = Instantiate(foodPrefab, transform); //create new instance
            food.SetActive(false);
            foodsNotInUse.Add(food);
        }
    }

    /// <summary>
    /// Destroys the previousfoods and returns a new list of instantiated foods
    /// </summary>
    /// <param name="previousFood"></param>
    /// <returns></returns>
    public List<GameObject> GetNewFoods()
    {
        List<GameObject> newFoods = new List<GameObject>();

        for (int i = 0; i < foodPerSnake; i++)
        {
            foodsNotInUse[0].SetActive(true);
            foodsNotInUse[0] = RepositionFood(foodsNotInUse[0]);

            newFoods.Add(foodsNotInUse[0]);
            foodsNotInUse.RemoveAt(0);
        }

        return newFoods; //return the newly made list
    }

    public GameObject RepositionFood(GameObject foodToReposition)
    {
        float randomX = Random.Range((-mapSizeX + mapXOffset) / 2, (mapSizeX + mapXOffset) / 2); //randomize the x cordinate
        float randomY = Random.Range((-mapSizeY + mapYOffset) / 2, (mapSizeY + mapYOffset) / 2); //randomize the y cordinate
        foodToReposition.transform.position = new Vector2(randomX, randomY);   //set the poistion to the newly generated random cordinates

        return foodToReposition;
    }

    public void RemoveFoods(List<GameObject> foodToHide)
    {
        for (int i = 0; i < foodToHide.Count; i++)
        {
            foodToHide[i].SetActive(false);
            foodsNotInUse.Add(foodToHide[i]);
        }
    }
}
