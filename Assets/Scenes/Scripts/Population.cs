﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Population : MonoBehaviour {

    [Header("Setup")]
    public GameObject snakePrefab;  //Snake prefab

    [Header("Population values"), Range(1, 100)]
    public int populationSize;  //The size of the population
    public bool loadBestNetwork;    //if we should use the best saved network as our starting network

    [Header("Snake values"), Range(1, 3)]
    public float snakeMovementSpeed; //speed of the sankes
    [Range(150, 360)]
    public float snakeTurnSpeed; //turn speed of the snakes
    [Range(5, 20)]
    public float lifespan;  //how long the snakes live without finding food

    [Header("Observations"), Range(10, 60)]
    public int observationCount; //how many rays we cast within our observation arc
    [Range(45, 270)]
    public float observationArc; //the field of view of the snakes (in degrees)

    [Header("UI")]
    public Text generationText; //shows the current generation
    public Text previousBestText; //shows the previous best
    public Text overallBestText; //shows the overall best, takes into account the one loaded from the file if in use

    [Header("Time"), Range(1, 5)]
    public float timeScale; //time scale modifier

    private NeuralNetwork theBestNetwork; //best overall network
    private NeuralNetwork startingNetwork; //the starting network
    private List<Snake> population = new List<Snake>(); //current population
    private int[] neurons; //how big the neuron array should be to initialize the neural network

    private int Generation { set {
            _Generation = value; //set generation
            generationText.text = _Generation.ToString(); //set the generation text
            if (population.Count > 0) //if we have someone in the population
                previousBestText.text = population[0].GetFitness().ToString("0.00"); //we set the prevous best text
        }
    }
    private int _Generation;

    private void Start()
    {
        Time.timeScale = timeScale; //Set timescale
        FindObjectOfType<FoodManager>().InitializeFoods(populationSize);

        neurons = new int[] { observationCount * 3 + 3, 32, 16, 8, 1 }; //Initialize neurons to match our required size

        theBestNetwork = LoadNeuralNetwork();        //Load previous best network

        if (loadBestNetwork)    //If we want to use the best network
        {
            startingNetwork = theBestNetwork;   //Set it as the starting network
            overallBestText.text = theBestNetwork.bestFitness.ToString("0.00"); //Set the corresponding text 
            Debug.Log("Loaded");
        }
        else
            startingNetwork = new NeuralNetwork(neurons); //Otherwise startingnetwork is new random network


        InitializePopulation();
    }
    private void Update()
    {
        if (!SnakesExist()) //If our generation is dead
            NewGeneration();    //Create a new one
    }
    /// <summary>
    /// Initializes the population
    /// </summary>
    private void InitializePopulation()
    {
        Generation = _Generation + 1;

        for (int i = 0; i < populationSize; i++) //For the size of our wanted population
        {
            Snake snake = Instantiate(snakePrefab, transform).GetComponent<Snake>();    //Instantiate the snake
            snake.Init(new NeuralNetwork(startingNetwork), snakeMovementSpeed, snakeTurnSpeed, observationCount, observationArc, lifespan); //initialize it
            snake.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));    //Set its starting rotation to random rotation
            population.Add(snake);  //Add him to the population
        }
    }
    /// <summary>
    /// Generates the new generation and mutates it
    /// </summary>
    private void NewGeneration()
    {
        population.Sort(ByFitness); //First we sort the population by their fitness value
        List<Snake> newGeneration = new List<Snake>();  //Temporary list for new population

        Snake bestPerformer = population[0]; //Select the best performer from the previous population

        if (theBestNetwork == null || bestPerformer.GetFitness() > theBestNetwork.bestFitness) //if the new best is better than the one in overall best network
        {
            theBestNetwork = new NeuralNetwork(bestPerformer.neuralNetwork); //we make the best network to be equal to the new best
            theBestNetwork.bestFitness = bestPerformer.GetFitness();    //set its fitness
            overallBestText.text = theBestNetwork.bestFitness.ToString("0.00"); //update our corresponding text field
            SaveNeuralNetwork(theBestNetwork); //save it to disk
        }

        while (newGeneration.Count < populationSize)
        {
            Snake snake = Instantiate(snakePrefab, transform).GetComponent<Snake>(); //instantiate the new snake
            snake.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));    //add random rotation
            snake.Init(new NeuralNetwork(bestPerformer.neuralNetwork), snakeMovementSpeed, snakeTurnSpeed, observationCount, observationArc, lifespan); //initialize him
            if (newGeneration.Count > 0)    //the first guy that we add, we dont want to mutate at all
                snake.neuralNetwork.Mutate(bestPerformer.GetFitness()); //Otherwise we mutate the new snakes neural network
            else
                snake.SetColor(Color.red); //otherwise we set the first guy to be a different color
            newGeneration.Add(snake);//Add him to the new generation
        }

        Generation = _Generation + 1; //Increment our generation value by one

        TerminatePopulation(); //Destroy old population
        population = newGeneration; //Make the new generation to be the current one
    }
    /// <summary>
    /// Destroys the current population
    /// </summary>
    private void TerminatePopulation()
    {
        for (int i = 0; i < population.Count; i++)
        {
            Destroy(population[i].gameObject);
        }
        population.Clear();
    }
    /// <summary>
    /// Returns true if there are snakes alive in population
    /// </summary>
    /// <returns></returns>
    private bool SnakesExist()
    {
        for (int i = 0; i < population.Count; i++)
        {
            if (!population[i].isDead)
                return true;
        }
        return false;
    }
    /// <summary>
    /// Sort method that sorts snakes by fitness in descending order
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private int ByFitness(Snake a, Snake b)
    {
        if (a.GetFitness() > b.GetFitness())
            return -1;
        else if (a.GetFitness() < b.GetFitness())
            return 1;
        else
            return 0;
    }
    /// <summary>
    /// Saves the neural network to disk to location "persistentdatapath/neuralnetworks/bestnetwork"
    /// </summary>
    /// <param name="network"></param>
    private void SaveNeuralNetwork(NeuralNetwork network)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/NeuralNetworks"))
            Directory.CreateDirectory(Application.persistentDataPath + "/NeuralNetworks");
        else
            File.Delete(Application.persistentDataPath + "/NeuralNetworks" + "/BestNetwork");

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/NeuralNetworks" + "/BestNetwork");
        bf.Serialize(file, network);
        file.Close();
    }
    /// <summary>
    /// Loads the best network from "persistentdatapath/neuralnetworks/bestnetwork", returns null if there is none
    /// </summary>
    /// <returns></returns>
    private NeuralNetwork LoadNeuralNetwork()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/NeuralNetworks"))
            Directory.CreateDirectory(Application.persistentDataPath + "/NeuralNetworks");
        else if (!File.Exists(Application.persistentDataPath + "/NeuralNetworks" + "/BestNetwork"))
            return null;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/NeuralNetworks" + "/BestNetwork", FileMode.Open);
        NeuralNetwork network = (NeuralNetwork)bf.Deserialize(file);
        file.Close();

        return network;
    }
}
