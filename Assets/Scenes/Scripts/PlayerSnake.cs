﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSnake : MonoBehaviour {

    [Header("Setup")]
    public GameObject bodyPartPrefab; //body part prefab

    private float speedMultiplier; //movement speed
    private float turnMultiplier; //turn speed
    private List<GameObject> myFoods; //our valid foods
    private List<GameObject> bodyParts = new List<GameObject>(); //our body parts
    private Rigidbody2D rigidBody; //our rigidbody
    private Vector2 velocity; //the movement velocity
    private PlayerController controller; //the player controller to send data to

    public void Init(PlayerController controller, float speedMultiplier, float turnMultiplier)
    {
        //set values
        this.speedMultiplier = speedMultiplier;
        this.turnMultiplier = turnMultiplier;
        this.controller = controller;

        //get components
        rigidBody = GetComponent<Rigidbody2D>();
        //get new foods from foodmanager
        myFoods = FindObjectOfType<FoodManager>().GetNewFoods();
    }

    private void Update() //called every fram
    {
        UpdateBodyPartPositions(); //we want to update our body parts position
    }

    private void FixedUpdate() //called every physics engine tick
    {
        rigidBody.MovePosition(rigidBody.position + (Vector2)transform.up * Time.deltaTime * speedMultiplier); //move forwards (or up)
        rigidBody.angularVelocity = Input.GetAxisRaw("Horizontal") * -turnMultiplier; //if we get input a or d / left arrow or right arrow we turn left/right
    }

    /// <summary>
    /// Updates the body parts position relative to head
    /// </summary>
    private void UpdateBodyPartPositions() //you can check detailed explanation of this from Snake script
    {
        for (int i = 0; i < bodyParts.Count; i++)
        {
            Transform current = bodyParts[i].transform;
            Transform previous = null;

            if (i == 0)
                previous = transform;
            else
                previous = bodyParts[i - 1].transform;

            float distance = Vector2.Distance(previous.position, current.position);
            float time = Time.deltaTime * distance / 0.1f * speedMultiplier;
            if (time > 0.5f)
                time = 0.5f;

            current.position = Vector2.Lerp(current.position, previous.position, time);
            current.rotation = Quaternion.Lerp(current.rotation, previous.rotation, time);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.IsChildOf(transform) || collision.gameObject.tag == "Wall") //if we collide with our tail or with wall
            Terminate(); // we die
        else if (collision.gameObject.tag == "Food") //if we find food
            FoundFood(collision.gameObject); //we found nom noms
    }
    /// <summary>
    /// call to kill the snake
    /// </summary>
    private void Terminate()
    {
        controller.PlayerDead(); //let the controller know we died
        FindObjectOfType<FoodManager>().RemoveFoods(myFoods); //tell the food manager to destroy our foods
    }
    private void FoundFood(GameObject food)
    {
        GameObject bodyPart = Instantiate(bodyPartPrefab, transform); //create new body part, check details from "Snake" script

        if (bodyParts.Count > 0)
        {
            bodyPart.transform.position = bodyParts[bodyParts.Count - 1].transform.position - bodyParts[bodyParts.Count - 1].transform.up * 0.2f;
            bodyPart.transform.rotation = bodyParts[bodyParts.Count - 1].transform.rotation;
        }
        else
        {
            bodyPart.GetComponent<Collider2D>().enabled = false;
            bodyPart.transform.position = (Vector2)transform.position - (Vector2)transform.up * 0.2f;
            bodyPart.transform.rotation = transform.rotation;
        }
        bodyPart.GetComponent<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;
        bodyParts.Add(bodyPart);

        controller.FoundFood(); //let the controller know we found food
        FindObjectOfType<FoodManager>().RepositionFood(food); //get new foods
    }
}
